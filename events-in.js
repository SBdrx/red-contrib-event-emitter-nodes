module.exports = function (RED) {
    function EventsIn(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var name = config.name || "demo";

        var listener = function (msg) {
            node.send({event: name, payload:msg});
        };
        RED.events.on(name, listener);

        node.on("close", function (done) {
            RED.events.off(name, listener);
        });
    }

    RED.nodes.registerType("events-in", EventsIn);
};


